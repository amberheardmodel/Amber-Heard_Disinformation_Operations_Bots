# Amber-Heard_Disinformation_Operations_Bots
<b><i>Social Network Analysis of Disinformation/Influence Operations and Bots</i> Cross-Platforms on <i>Amber Heard</i>. 
<br>Twitter, Reddit, YouTube, Instagram, Change.org, and Facebook for <i>6 Social Media Platforms</i>.</b>

#### <i><b>Millions of Texts and Accounts were studied for Social Network Analysis.</b></i>

We're Worldwide Independent Researchers with over 20 data analysts, scientists, and observers analyzing this data for over a year. Research and development is ongoing, and this public operation spanning for years continues. We invite researchers and students to study this public case example. 

#### <i><b>Data collected on Amber Heard is primarily related to the years 2018-2021, ongoing into 2022.</b></i>

This is the most detailed public analysis of a disinformation operation and the most voluminous yet - showing the impact of <a href="https://www.brookings.edu/research/the-breakout-scale-measuring-the-impact-of-influence-operations"><i>Category 6</a> influence,</b></i> globally affecting the public, businesses, nonprofits, and entities beyond the victim. This work is in-progress and prepared for peer-review.  

<b>Our Purpose is to create Precedents and Foundations to help other victims of cyberabuse, bots, domestic abuse, <a href="https://www.connecticutprotectivemoms.org/coercive-control-legislation-in-the">coercive control</a>, hostile environments, and Disinformation Operations to improve lives.</b> 
<br>We want to save lives and help partners create systems to help online - e.g., specialized and accurate rescue, quality custom studies, data analysis, Social Network Analysis, forensics, research, situation awareness, data science projects, and rescue/public safety technologies - <i>with focus on the victim primarily and <b>her environment</b></i>.

- <a href="https://www.teamscopeapp.com/blog/6-repositories-to-share-your-research-data"><i><b>Public Research</b></a> - This study and its data is fully open for scientific learning, developing solutions, saving lives, maximizing knowledge, blueprints, and archiving for long-term benefit.</i>
<br>- If a file of analysis is too large to preview on github, please download, or preview in this <a href="https://drive.google.com/drive/folders/18MWGm7dRX6Gi5UPo0fO9xQMtT4eHIrbn?usp=sharing">google folder</a>.
<br>- <b>Full Data</b> sharing is in-progress on <a href="https://drive.google.com/drive/folders/1GjKdNpsJSBIX9MsQgKq2FGTArDVEYFFI?usp=sharing">Google</a>, <a href="https://www.kaggle.com/rescuesocialtech/datasets">Kaggle</a>, Archives, <a href="https://www.scribd.com/user/598949302/Rescue-Social-Tech/uploads">Scribd</a> and public research data repositories.</i>

### <b>Amounts of Data Collections [Updates]:</b><br>
- Twitter: 985,400 Tweets and 275,567 Accounts Jan 2018-April 2021, (2) Over 880,000 Tweets of Top Users, (3) 700K+ Retweets, Links
- Reddit: 164,530 Contributions, 15,896 Submissions, 71,319 Accounts, Links
- Instagram: 1,751,113 Comments, 193,967 Posts, 717,311 Accounts (Posts made by 36,137 accounts and 681,174 commented), Links
- YouTube: (1) 2,176,926 Comments (1,693,341 Comments with 483,585 Replies), 6,893 Videos (with dislike ratios), 1,152,868 Accounts, Cross-Platform Links, (2) 11,152 videos, 324,029 comments
- Change.org: 28,952 Comments, 181 Petitions, 18,908 Milestones, Cross-Platform Links
- Facebook: Page Comments and Reviews, Links in Groups for Cross-Platforms 
- Crossplatform for Groups in Facebook, Reddit, and Channels of YouTube<br>

### <b>Index and Contents:<br></b>
<i>Use for Timeline Correlations to Risks and Preliminary Effects</i>
- <b>Analysis of Social Media Platforms</b> - Complex Study with Detailed Python Notebooks
<i><br>Twitter, Reddit, Instagram, Change.org, YouTube, Facebook, Cross-Platforms
<br>Peaks, Anomalies, Timelines, Patterns, Statistical Models
<br>Timings, Graphs, Bot Networks, NLP/NLU and Sentiment Analysis
<br>Banned, New, Unverified Account Layers
<br>Investigations of Peaks, Accounts, Comments, Postings
<br>Repeated Texts with Timings, Multiple Accounts, Community Detection
<br>Coordination and Gamifications
<br>Threat Analysis and Negative Texts Heat Maps
<br>Metrics, Likes/Dislikes, UpScores/DownScores, BotScores
<br>Links Analysis, Urls, and NLP Analysis Cross-Platforms</i>
- <b>Anomalies and Comparisons</b>
- <b>Reports on Analysis and Platforms:</b>
<br><i>Summaries of Analysis, Years, Timelines, Accounts, Timings, NLP, Peaks</i>
<br><i>Showing Coordinated Activity and Anomalies of Disinformation Operations</i>
- <b>Images/Highlights/Figures</b>
- <b>Data Samples</b>
- <b>Banned, Deleted, Suspended Accounts</b>
- <b>Research Papers</b>
- <b>NLU Classifications, Monitoring, AI</b>
- <b>Positive NLG Compliments, Prompts, AI</b>
- <b>Analysis Guides</b>
- <b>Background</b> 

<b>Positive NLG - Natural Language Generation - Compliments Prompt:</b><br>
Compliments Generation on Amber Heard based on Jason Momoa<br>
 https://github.com/semiosis/prompts/blob/master/prompts/compliment-generation-based-on-a-celebrity-1.prompt<br>
- 10,000 Compliments on Amber Heard made in 2 hours: https://asciinema.org/a/w5ayuAEGGpgsxTCKWbAGRPfvZ<br>
- 3-Way Bots chat with Actress Amber Heard, Actor Tom Cruise, and Director Christopher Nolan demonstrated at: https://semiosis.github.io/apostrophe <br>
- Code and Demos from AI Researcher at: https://github.com/mullikine/positive-nlg-compliments<br>
- <a href="https://github.com/semiosis">Semiosis</a> is a free and open-source curation of prompts for OpenAI's GPT-3/Codex, EleutherAI's GPT-j, AlephAlpha's World Model and other language models.<br>

### <b>Collaboration:</b> 
If you're a social enterprise, researcher, organization, student, or academic helping female victims of abuse, coercive control, domestic abuse, bots, harassment, or hostile environments - we welcome partnership with you. 
<br>Our studies and data can be used for Social Network Analysis, Data Analysis, machine learning, Natural Language Processing, artificial intelligence, Data Science labeling, anti-abuse, rescue projects, anti-crime, timelines, OSINT, simulations, and monitoring systems to help other victims and their lives. 
<br><br>Since this data is high volume about domestic abuse and gender, with an actress and film industry element, it's highly relevant for classification and data science projects on those topics.
<br><i>Please contact us for data and code.</i>
<br><br>
<b>Although it's abuse of process, <a href="https://www.connecticutprotectivemoms.org/coercive-control-legislation-in-the">coercive control</a> is becoming illegal in more countries and states/provinces worldwide.</b>
- <i>If you're a victim needing help against cyberabuse especially in <a href="https://safeescape.org">online domestic abuse</a> and <a href="https://metta-space.com">hostile environments</a>, please contact us.</i><br>

#### <b>Do No Harm:</b>
All collaborators, researchers, and observers are required to sign, acknowledge, agree to "Do No Harm" agreements. If you are one of the victims, businesses, nonprofits, or entities listed in this study and want to collaborate with us, please contact us.
<br><br>We make this study and data fully open to <a href="https://www.teamscopeapp.com/blog/6-repositories-to-share-your-research-data">Public Research</a>. We do not include the full code, however, we make data open on data repositories to protect the individual in this study, to make a long-lasting impact for learning in the scientific world, and to prevent harmful operations. As a precedent, other researchers and students should use this GitHub and study as a basis for their own social network analysis projects. We draw attention to this project months before in-person appearances for safety.
<br>

### <b>Data Samples and Full Data [Updates]:</b><br>
On GitHub:
- 5 Months of Data for Cross-Platform Analysis:
- 307,649 Tweets of Twitter, Texts of Accounts in Coordination, 674,469 Retweets, and All Listed Accounts
- 43,509 Contributions of Reddit and All Listed Accounts
- 394,237 Comments of Instagram and All Listed Accounts
- 28,952 Comments of Change.org, 18,908 Milestones, and 181 Petitions Data
- 6,893 YouTube Videos Data, 424,457 Comments of YouTube, "Adapt & Survive" 130K Comments, All Listed Accounts
- 5.9K+ Labeled NLU Data of Support, Defense, Compliments and Training Data
- Threat Analysis Data and NLU Test Data for Instagram, Reddit, and Twitter
- Anomolies Data Samples
- 10,168 Compliments of Amber Heard from AI NLG Prompts
- Links/URLs Lists of comments/postings of Instagram, YouTube, Twitter, Reddit, Change.org

<b>Public Research:</b>
<br>This Study is open for Public Research, scientific learning, and archiving.
<br>Full Data is on <a href="https://drive.google.com/drive/folders/1GjKdNpsJSBIX9MsQgKq2FGTArDVEYFFI?usp=sharing">Google</a>, and soon added to Kaggle, Archives, and research data repositories.
<br><i>"Research data can save lives, help develop solutions and maximise our knowledge. Promoting collaboration and cooperation among a global research community is the first step to reduce the burden of wasted research."</i>

<b>Cross-Platforms Analysis:</b><br>
Analysis is in-progress and more will be added after published research papers for researcher privacy.
<br><i>Data of 5 months of December 2020 to April 2021 is provided for open peer-review and research studies on GitHub. Full Data for more independent cross-platform analysis is provided in research data repositories.</i>
<br><i>- Note:</i> Peaks, Heat Maps of Threats, coordinated bot activities, repeated same texts, and more anomalies show for 2018-2022 continuing, especially February 2020 onward as seen in analysis files.

<i>The Academic API was used for Retweets and <a href="https://InformationTracer.com">InformationTracer</a> for CrowdTangle API.</i><br>

### <b>Background:</b><br>
Multiple Research Papers on Disinformation/Influence Operations Cross-Platforms<br>
August 2020 Counterclaims on Bots: https://www.scribd.com/document/473092071/cl-2019-2911-def-counterclaims-8-10-2020<br>
Influence Operation of 6,000 Twitter accounts found by BotSentinel: https://youtu.be/bb2bC04OEPw<br>

#### <b>Related Articles for Understanding:</b><br> 
<i>"Disinformation in the Private Sector: The Price of Influence"</i>
- New Accounts Layers, Aged Accounts, Created News-Sites:</br>
https://go.recordedfuture.com/hubfs/reports/cta-2019-0930.pdf<br>
- Psy-Group's Bots - https://www.newyorker.com/magazine/2019/02/18/private-mossad-for-hire<br>
- Devumi Bots - https://www.thebureauinvestigates.com/stories/2017-12-07/twitterbots<br>
- BlackCube vs Actresses: https://www.newyorker.com/news/news-desk/harvey-weinsteins-army-of-spies<br>
- Medium "Bots Created Anti-Amber Heard" Operations: https://medium.com/@aquaman-bots/how-social-bots-created-an-anti-amber-heard-aquaman-campaign-e68e16637d3a<br>
- Actress Meghan Markle Operations: (1) https://laptrinhx.com/meghan-markle-s-twitter-bot-network-the-whole-thing-is-a-bit-insane-2560404981 (2) https://www.newsweek.com/meghan-markle-troll-accused-posing-black-woman-second-account-suspended-twitter-bot-sentinel-1652271<br>
- Jessica Reisinger-Raubenolt & Lilia Operations - https://www.tampabay.com/news/crime/2021/08/05/cameron-herrin-went-to-prison-for-a-tampa-crash-were-the-tweets-that-followed-real<br>
- Video - Rose McGowan vs BlackCube: https://www.youtube.com/watch?v=tv6ewJQ7L3k<br>
- Amber Heard, Cyrillic Spiders from Mars - https://www.thegeekbuzz.com/the-basement/cyrillic-russian-spiders-from-mars


### <b>Scientific Methods:</b>
Worldwide Data Scientists, Researchers in Cyber Intelligence, PhDs, Data Analysts, and a former Google Research Intern collected and analyzed data. We use the scientific method of testing.
<br>
- <i>Scientific Method: Observation, Question, Hypothesis, Experiment, Results, Conclusion 
 <br>- Limit Variables and Repeat Same Methods on all platforms. 
<br>- Cross-checking multiple times with full field JSONs and CSVs with processes in data science.</i>
- <i><b><a href="https://www.brookings.edu/research/the-breakout-scale-measuring-the-impact-of-influence-operations/">The Breakout Scale: Measuring the Impact of Influence Operations</a> by <a href="https://twitter.com/benimmo/status/1309532354306879488">Ben Nimmo</a> in Social Network Analysis Standards:</b></i><br>
"Category One operations only spread within one community on one platform, while Category Two operations either spread in one community across multiple platforms, or spread across multiple communities on one platform. Category Three operations spread across <i>multiple social media platforms and reach multiple communities.</i> 
 <br>Category Four operations break out from social media completely and are amplified by <i>mainstream media</i>, while Category Five operations are amplified by <i>high-profile individuals</i> such as celebrities and political candidates. 
 <i><br>An IO reaches <b>Category Six</b> if it triggers a policy response or some other form of concrete action, or if it includes <b>a call for violence.</i></b>"
- <i><a href="https://en.wikipedia.org/wiki/Situation_awareness">Situation Awareness:</a><br>
Objective	Tactical (short-term):	situational assessment	- situation awareness<br>
Strategic (long-term):	sensemaking -	understanding<br>
Scientific (longer-term):	analysis	- prediction<br></i>
- <i><a href="https://www.teamscopeapp.com/blog/6-repositories-to-share-your-research-data">Public Research:</a>
<br>"When researchers make their data public, they increase transparency and trust in their work, they enable others to reproduce and validate their findings, and ultimately, contribute to the pace of scientific discovery by allowing others to reuse and build on top of their data."</i>

### <b>Researchers:</b>
<b>We welcome Researchers to work with this Data on Amber Heard to create Blueprints on how Bots and Disinformation/Influence Operations work. </b>
<br>Please ask to collaborate with us at rescuesocial@gmail.com.<br>
- <i><b>Research and building technologies is Ongoing as the operation is Continuing over years and this is a Public Github Example. </b>
<br>We are continuing analysis and data collections, including creating technologies for systems on <a href="https://www.connecticutprotectivemoms.org/coercive-control-legislation-in-the">coercive control.</a></i>
<br><i>“Since the fate of the world is non-existence, Since You Exist, Be Merry.” - Persian Mathematician <a href="https://en.wikipedia.org/wiki/Omar_Khayyam">Omar Khayyam</a></i>

### <b>Citing Research Project:</b><br>
If interested in citing our research project in your studies, publication, journal, database, or article, please provide a link to this GitHub repository with its title and contact us for collaboration.


